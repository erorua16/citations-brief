## TO START

1. clone or install project
2. run in terminal `npm i`
3. run in terminal `composer install`
4. rename the `.env-example` file to `.env`
5. in your new `.env` file  edit:  
`DB_DATABASE= DATABASE`
`DB_USERNAME= USERNAME`
`DB_PASSWORD=PASSWORD`
and input your information.

6. run in terminal `php artisan key:generate`
To add an encrypted key to your .env, if it doesn't add the key in your .env file do it manually and paste the Key using the command in terminal `php artisan key:generate --show`

7. run in terminal `php artisan migrate --seed` to populate your database


## TERMINAL COMMANDS

1. `npm run dev` to compile 

2. `npm run watch` to compile app.js and app.css files in the Public folder

3. `php artisan serve` to launch server

## FRONTOFFICE

1. homepage is `http://127.0.0.1:8000/`. you can use the nav bar to navigate to the authors or the citations 
2. access all authors that have citations through `http://127.0.0.1:8000/authors`
3. accesss all citations through `http://127.0.0.1:8000/citations`
4. accesss all quotes from an author through `http://127.0.0.1:8000/idv-author/` and adding the id at the end

## BACKOFFICE

* to access admin page go to `http://127.0.0.1:8000/login` and login using the admin user credentials (email `admin@admin.com` password `admin`)
* to see all citations go to `http://127.0.0.1:8000/admin/citations`
* to add a citation go to `http://127.0.0.1:8000/admin/create-citation`
* to see all authors go to `http://127.0.0.1:8000/admin/authors`
* to add a citation go to `http://127.0.0.1:8000/admin/create-author`


