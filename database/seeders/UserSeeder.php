<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        // Admin user
      DB::table('users')->insert([
          'name' => 'admin',
          'email' => 'admin@admin.com',
          'role' => 'admin',
          'password' => Hash::make('admin'),
          'created_at' => date("Y-m-d H:i:s"),
      ]);

    }
}
