<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\CitationsApiService;
use App\Models\Authors;

class CitationsSeeder extends Seeder
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $citationsApi;
/**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(CitationsApiService $api)
    {
        $this->citationsApi = $api;
    }

   function run()
    {
        $result = $this->citationsApi->citationsApi();
        $result = $result->quotes;
        $i = 0;
        forEach($result as $res) {
            if($i < 100){
                if( $res->author != "Anonymous" && $res->author != "Anonymousk"){
                    $i ++;
                    $author = DB::table('authors')
                    ->where('name', '=', $res->author)
                    ->value('id');
                    DB::table('citations')->insert([
                        'quote' => $res->text,
                        'authors_id'=> $author,
                        'created_at' => date("Y-m-d H:i:s"),
                    ]);
                }
            }
        }
    }
}
