<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\CitationsApiService;

class AuthorsSeeder extends Seeder
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $citationsApi;
/**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(CitationsApiService $api)
    {
        $this->citationsApi = $api;
    }

   function run()
    {
      $result = $this->citationsApi->authorsApi();
      $result = $result->authors;
      forEach($result as $res) {
          DB::table('authors')->insert([
              'name' => $res->name,
              'created_at' => date("Y-m-d H:i:s"),
          ]);
      }
    }
}
