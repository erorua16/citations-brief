@extends('layouts.front')

@section('content')
<div class="page">

    <div class="containerFront">
        
        <div class="titlePage">
        <h1>All Quotes</h1>
        </div>

        <div class="list">

            @foreach ($result as $citation)

            <div class="citation">
                <div class="quote">
                    <h1>"</h1>
                    <h5>
                        {{ ($citation->quote) }}
                    </h5>
                    <h1 >"</h1>
                </div>
                <a href="{{ route('idv-author', [$citation->author->id]) }}"><p>
                - {{ ($citation->author->name) }}
                </p></a>
            </div>

            @endforeach

        </div>

        <div class="paginatorDiv">
            {{ $result->onEachSide(2)->links() }}
        </div>

    </div>
</div>

@endsection