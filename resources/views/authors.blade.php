@extends('layouts.front')

@section('content')
<div class="page">

    <div class="containerFront">

    <form class="" action="{{ route('search-author-front') }}" method="GET">
        <input class="" type="text" name="search" placeholder="Input author name..."/>
        <button class=""type="submit">Search</button>
        <a href="{{ route('admin-author') }}" class="mt-3 btn btn-danger btn-sm">Back</a>
    </form>

        <div class="titlePage">
            <h1>All Authors</h1>
        </div>

        <div class="list">
            
            @foreach ($result as $author)

            <div class="citation">
                <div class="quote">
                <a href="{{ route('idv-author', [$author->id]) }}"><h5>
                    {{ ($author->name) }}
                </h5></a>
                
                </div>
            </div>

            @endforeach

        </div>

        <div class="paginatorDiv">
            {{ $result->onEachSide(2)->links() }}
        </div>

    </div>
</div>

@endsection