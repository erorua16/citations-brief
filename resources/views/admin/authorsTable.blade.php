@extends('layouts.app')

@section('content')
<div class="container">
    
    @if (session('status'))
        <h6 class="alert alert-success">{{ session('status') }}</h6>
    @endif

    <form class="container-fluid my-5" action="{{ route('search-author') }}" method="GET">
        <input class="container-fluid" type="text" name="search" placeholder="Input author name..." />
        <button class="mt-3 btn btn-primary btn-sm"type="submit">Search</button>
        <a href="{{ route('admin-author') }}" class="mt-3 btn btn-danger btn-sm">Back</a>
    </form>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Admin page: author list</h4>
                </div>
                <div class="card-body">
                @if($authors->isNotEmpty())
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Author</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($authors as $author)
                            <tr>
                                <td>{{ $author->name }}</td>
                                <td>
                                    <a href="{{ route('edit-author', [$author->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('delete-author', [$author->id]) }}" method="post">
                                    <input class="btn btn-danger btn-sm" type="submit" value="Delete" />
                                        @method ('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else 
                        <div>
                            <h2>No authors found</h2>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="paginatorDiv">
        {{ $authors->onEachSide(2)->links() }}
    </div>

</div>


@endsection