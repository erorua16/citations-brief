@extends('layouts.app')

@section('content')

<div class="container">
    @if (session('status'))
        <h6 class="alert alert-success">{{ session('status') }}</h6>
    @endif
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4>Add a Citation
                        <a href="{{ route('admin-citation') }}" class="btn btn-danger float-end">BACK</a>
                    </h4>
                </div>
                <div class="card-body">

                    <form action="{{ route('add-citation')}}">
                        @csrf
                        @method('PUT')

                        <div class="form-group mb-3">
                            <label for="">Quote</label>
                            <input type="text" name="quote" placeholder="input a citation" class="form-control">
                        </div>

                        <select class="form-select" aria-label="Default select example" name="author">
                            @foreach($result as $author)
                            <option value="{{ $author->id }}">{{$author->name}}</option>
                            @endforeach
                        </select>
                        
                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-primary">Add citation</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection