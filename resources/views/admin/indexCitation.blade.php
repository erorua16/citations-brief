@extends('layouts.app')

@section('content')
<div class="container">
    
    @if (session('status'))
        <h6 class="alert alert-success">{{ session('status') }}</h6>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Admin page: citation list</h4>
                </div>
                <div class="card-body">
                @if($citations->isNotEmpty())
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Quote</th>
                                <th>Author</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($citations as $quote)
                            <tr>
                                <td>{{ $quote->quote }}</td>
                                <td>{{ $quote->author->name }}</td>
                                <td>
                                    <a href="{{ route('edit-citation', [$quote->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('delete-citation', [$quote->id]) }}" method="post">
                                    <input class="btn btn-danger btn-sm" type="submit" value="Delete" />
                                        @method ('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else 
                        <div>
                            <h2>No citations found</h2>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="paginatorDiv">
        {{ $citations->onEachSide(2)->links() }}
    </div>
</div>
@endsection