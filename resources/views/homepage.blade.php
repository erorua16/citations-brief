@extends('layouts.front')

@section('content')
<div class="page">
    <div class="containerFront">
        <div class="titlePage">
            <h1>Welcome to citations!</h1>
        </div>

        <div class="citation">
            
            <h3>Latest quote</h3>
            <div class="quote">
                <h1>"</h1>
                <h5>
                    {{ ($result->quote) }}
                </h5>
                <h1 >"</h1>
            </div>
            <a href="{{ route('idv-author', [$result->author->id]) }}"><p>
                - {{ ($result->author->name) }}
                </p></a>
            
        </div>
    </div>

</div>
@endsection
