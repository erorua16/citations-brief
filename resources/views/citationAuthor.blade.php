
@extends('layouts.front')

@section('content')

<!-- <div>
    <form action="{{ route('search-author')}}" method="GET">
        <input type="text" name="search_author" placeholder="Search author..">
    </form>
</div> -->

<div class="page">

    <div class="containerFront">
        
        <div class="titlePage">
        <a href="{{ url()->previous() }}">
        Back
        </a>
            <h1>{{ session('result_author') }} quotes</h1>
        </div>
        @if($result->isNotEmpty())
        <div class="authorQuotesList">
                @foreach ($result as $citation)

                <div class="citation">
                    <div class="quote">
                        <h1>"</h1>
                        <h5>
                            {{ ($citation->quote) }}
                        </h5>
                        <h1 >"</h1>
                    </div>
                    <p>
                        - {{ ($citation->author->name) }}
                    </p>
                </div>

                @endforeach
        </div>
        @else
        <h5>No Quotes</h5>
        @endif

        <div class="paginatorDiv">
            {{ $result->onEachSide(2)->links() }}
        </div>

    </div>
</div>


@endsection