<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Authors;
use App\Models\Citations;
use Illuminate\Support\Facades\DB;

class AuthorBackController extends Controller
{
   //Function to show all authors
   public function index()
   {
       $authors = Authors::paginate(10);
       return view('admin.authorsTable', ['authors' => $authors]);
   }


   //Function to input edits to author
   public function edit($id)
   {
       $author = Authors::find($id);
       return view('admin.editAuthor', compact('author'));
   }


   //Function to push edit to database
   public function update(Request $request, $id)
   {

    if (Authors::where(['name' => $request->input('name')])->exists() == false) {
        $author = Authors::find($id);
        $author->name = $request->input('name');
        $author->update();
        return redirect()->route('admin-author')->with('status','author has been successfully updated');
    }
    else {
        return redirect()->back()->with('status','Author already exists, change to a different name');
    }
    
   }


   //Function to delete author
   public function delete($id)
   {
        if (Citations::where(['authors_id' => $id])->exists()) {
            Citations::where(['authors_id' => $id])->delete();
        }
       $author = Authors::find($id);
       $author->delete();
       return redirect()->back()->with('status','author has been successfully deleted');
   }

   //Function to search authors
   public function search(Request $request)
   {
       $search = $request->input('search');
       $authors = Authors::query()
           ->where('name', 'LIKE', "%{$search}%")
           ->paginate(10);
       return view('admin.authorsTable', compact('authors'));
   }

   public function add(Request $request){
        $author = $request->input('name');
        if (Authors::where(['name' => $author])->exists()) {
            return redirect()->back()->with('status'," $author already exists");
        }

        $newAuthor = new Authors();
        $newAuthor->name = $author;
        $save = $newAuthor->save();
        return redirect()->back()->with('status'," $author has been added");
        
    }
}
