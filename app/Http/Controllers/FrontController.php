<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Models\Authors;
use App\Models\Citations;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    
    public function index()
    {
        $result = Citations::latest()->first();
        return view('homepage', compact('result'));
    }

    public function citations()
    {
        $result = Citations::paginate(10);
        return view('citations', compact('result'));
    }

    public function authors()
    {
        $result = Authors::has('citation')->paginate(10);
        return view('authors', compact('result'));
        
    }

    //Function to search authors
    public function search(Request $request)
    {
        $request->session()->forget('result_author');
        $search = $request->input('search');
        $result = Authors::query()
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(10);
        $request->session()->put(['result_author' => $search]);
        return view('authors', compact('result'));
    }

    public function idvAuthor(Request $request, $id)
    {
        $request->session()->forget('result_author');
        $result = Citations::query()
        ->where('authors_id', '=', $id)
        ->paginate(10);
        $authorName = Authors::find($id);
        $request->session()->put(['result_author' => $authorName->name]);
        return view('citationAuthor', compact('result'));

    }
}
