<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Authors;
use App\Models\Citations;
use Illuminate\Support\Facades\DB;

class BackController extends Controller
{
      //Function to show all citations
      public function index()
      {
          $citations = Citations::paginate(10);
          return view('admin.indexCitation', ['citations' => $citations]);
      }
  
  
      //Function to input edits to citation
      public function edit($id)
      {
          $citation = Citations::find($id);
          return view('admin.editCitation', compact('citation'));
      }
  
  
      //Function to push edit to database
      public function update(Request $request, $id)
      {
          $citation = Citations::find($id);
          $citation->quote = $request->input('quote');
        //   $citation->email = $request->input('author');
          $citation->update();
          return redirect()->route('admin-citation')->with('status','citation has been successfully updated');
      }
  
  
      //Function to delete citation
      public function delete($id)
      {
          $citation = Citations::find($id);
          $citation->delete();
          return redirect()->back()->with('status','citation has been successfully deleted');
      }
  
      //Function to search citations
      public function search(Request $request)
      {
          $search = $request->input('search');
          $citations = Citations::query()
              ->where('quote', 'LIKE', "%{$search}%")
              ->orWhere('email', 'LIKE', "%{$search}%")
              ->get();
          return view('admin.indexCitation', compact('citations'));
      }

      //Function to display create citation page
      public function create() {
            $result = Authors::get();
            return view('admin.citationsAdd', compact('result'));
        }

      //Function to add citation
      public function add(Request $request){
        $citation = $request->input('quote');
        $author = $request->input('author');
        if (Citations::where(['quote' => $citation])->exists()) {
            return redirect()->back()->with('status'," $citation already exists");
        }

        $newCitation = new Citations();
        $newCitation->quote = $citation;
        $newCitation->authors_id = $author;
        $save = $newCitation->save();
        return redirect()->back()->with('status'," A new citation has been added");
        
    }
      
}
