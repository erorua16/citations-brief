<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Citations extends Model
{
    use HasFactory;

    // Determines which database table to use
    protected $table = 'citations';

    protected $fillable = [
        'quote', 'authors_id'
    ];
    
    public function author()
    {
        return $this->belongsTo(Authors::class, 'authors_id');
    }
}
