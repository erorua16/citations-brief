<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    use HasFactory;
    
    // Determines which database table to use
    protected $table = 'authors';

    protected $fillable = [
        'name'
    ];

    public function citation() 
    {
        return $this->hasMany(Citations::Class);
    }


}
