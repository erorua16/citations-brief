<?php

namespace App\Services;

class CitationsApiService 

{

    private $client;
    private $baseUrl;

    public function __construct(){
        $this->client = new \GuzzleHttp\Client();
        $this->baseUrl = 'https://goquotes-api.herokuapp.com/api/v1/all/';
    }

    public function citationsApi()

    {
        $url = $this->baseUrl . "quotes";
        $request = $this->client->get($url);
        $body = json_decode($request->getBody());
        return $body;

    }

    public function authorsApi()
    {
        
        $url =  $this->baseUrl . "authors";
        $request = $this->client->get($url);
        $body = json_decode($request->getBody());
        return $body;
        
    }

}

?>