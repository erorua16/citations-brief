<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//////////FRONT ROUTES//////////

Auth::routes();

//Route to latest post index
Route::get('/', [App\Http\Controllers\FrontController::class, 'index'])->name('homepage');

//Route to display all citation
Route::get('/citations', [App\Http\Controllers\FrontController::class, 'citations'])->name('citations');

//Route to display all authors
Route::get('/authors', [App\Http\Controllers\FrontController::class, 'authors'])->name('authors');

Route::get('/search-author-front', [App\Http\Controllers\FrontController::class, 'search'])->name('search-author-front');

//Route to display individual movie
Route::get('idv-author/{id}', [App\Http\Controllers\FrontController::class, 'idvAuthor'])->name('idv-author');



//////////////////ADMIN ROUTES//////////////////

//////////CITATIONS ROUTES//////////

//Route, to show table of citations and options
Route::get('/admin/citations', [App\Http\Controllers\BackController::class, 'index'])->middleware(['auth', 'role:admin'])->name('admin-citation');

//Route to input edited citation information
Route::get('/edit-citation/{id}', [App\Http\Controllers\BackController::class, 'edit'])->middleware(['auth', 'role:admin'])->name('edit-citation');

//Route to update the edited citation information to the database
Route::put('/update-citation/{id}', [App\Http\Controllers\BackController::class, 'update'])->middleware(['auth', 'role:admin'])->name('update-citation');

//Route to delete the selected citation
Route::delete('/delete-citation/{id}', [App\Http\Controllers\BackController::class, 'delete'])->middleware(['auth', 'role:admin'])->name('delete-citation');

//Route to search citation
Route::get('/admin/search-citation', [App\Http\Controllers\BackController::class, 'search'])->middleware(['auth', 'role:admin'])->name('search-citation');

//Route to go to page to create an author
Route::get('/admin/create-citation', [App\Http\Controllers\BackController::class, 'create'])->middleware(['auth', 'role:admin'])->name('create-citation');

//Route to add an citation to database
Route::get('add-citation', [App\Http\Controllers\BackController::class, 'add'])->middleware(['auth', 'role:admin'])->name('add-citation');

//////////AUTHOR ROUTES//////////

//Main route, show table of authors and options
Route::get('/admin/authors', [App\Http\Controllers\AuthorBackController::class, 'index'])->middleware(['auth', 'role:admin'])->name('admin-author');

//Route to input edited author information
Route::get('/edit-author/{id}', [App\Http\Controllers\AuthorBackController::class, 'edit'])->middleware(['auth', 'role:admin'])->name('edit-author');

//Route to update the edited author information to the database
Route::put('/update-author/{id}', [App\Http\Controllers\AuthorBackController::class, 'update'])->middleware(['auth', 'role:admin'])->name('update-author');

//Route to delete the selected author
Route::delete('/delete-author/{id}', [App\Http\Controllers\AuthorBackController::class, 'delete'])->middleware(['auth', 'role:admin'])->name('delete-author');

//Route to search author
Route::get('/admin/search-author', [App\Http\Controllers\AuthorBackController::class, 'search'])->middleware(['auth', 'role:admin'])->name('search-author');

//Route to go to page to create an author
Route::get('/admin/create-author', function () {
    return view('admin.authorsAdd');
})->middleware(['auth', 'role:admin'])->name('create-author');

//Route to add an author to database
Route::get('add-author', [App\Http\Controllers\AuthorBackController::class, 'add'])->middleware(['auth', 'role:admin'])->name('add-author');

